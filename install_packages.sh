#!/bin/bash

yay -Syu --needed \
	hyprland hyprpicker hyprshot xdg-desktop-portal-hyprland hyprpaper \
	cliphist swaylock swaybg eww-wayland wofi \
	ttf-material-symbols-variable-git \
	wl-clipboard wl-clip-persist \
	pipewire wireplumber \
       	dunst polkit-kde-agent \
	qt5-wayland qt6-wayland \
	alacritty playerctl ttf-material-icons-git


