#!/usr/bin/env bash

while ! /nix/store/$(ls -la /nix/store | grep polkit-kde-agent | grep '^d' | awk '{print $9}')/libexec/polkit-kde-authentication-agent-1; do
       sleep 5
done

